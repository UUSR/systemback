# systemback.sh

**System backup and restore script for Debian-based distributions**

This Bash script is requires a basic knowledge of the Linux command line interface!  
By default, all restore points in a storage directory are incremental and temporary. The oldests may be automatically deleted when the new ones are created.

## Table of contents

- [Requirements](#requirements)
- [Install](#install)
- [Change configuration settings](#change-configuration-settings)
- [Usage](#usage)
- [Rename and delete restore points](#rename-and-delete-restore-points)

## Requirements

Make sure that the following packages are installed in the system.

*procps  
psmisc  
rsync  
xterm*

```sh
sudo apt install procps psmisc rsync xterm
```

## Install

Place the '*systemback.sh*' under the '*/usr/local/bin*' directory and make it executable.

```sh
sudo wget -O /usr/local/bin/systemback.sh goo.gl/SXWYn7
sudo chmod +x /usr/local/bin/systemback.sh
```

The script basically a command line tool, but there is a clickable icon to quick-start the restore point creation and the restoration (from right-click menu).

```sh
mkdir -p ~/.local/share/{applicati,ic}ons
wget -O ~/.local/share/applications/systemback.desktop goo.gl/Vhe3z7
wget -O ~/.local/share/icons/systemback.png goo.gl/ms96CR
```

## Change configuration settings

*storage_dir  
max_rp_num  
max_size  
exclude*

```sh
sudo nano /usr/local/bin/systemback.sh
```

![](https://prohardver.hu/dl/upc/2018-04/180556_nano.png)

Press *Ctrl+O* to save the changes, and *Ctrl+X* to exit from nano.

## Usage

New restore points can be created with the *-n* option.

```sh
sudo systemback.sh -n
```

The system and the user's configuration files can be restored with the *-r* option. The second argument defines the restore point to be used. All restore points can be selected with its number. If the restore point is not set, the latest one will be used.

```sh
sudo systemback.sh -r

ls /storage/directory | grep ^SB
sudo systemback.sh -r 1
sudo systemback.sh -r 2
sudo systemback.sh -r 11
...
```

![](https://prohardver.hu/dl/upc/2018-04/180556_restore.png)

Broken system files can be repaired from a Live system. Just manually mount the system partition(s) under the '*/mnt*' directory, and use the *-l* option.

```sh
sudo mount /dev/sdXN /mnt
...
cd /storage/directory
sudo bash -c "$(wget -O- goo.gl/SXWYn7)" . -l
```

## Rename and delete restore points

The restore points will have a default name, but these can be renamed. In the filesystem, the names have a 5-character prefix, which should not be changed manually!

```sh
cd /storage/directory
ls | grep ^SB
sudo mv SB01_* "SB01_My first restore point"
```

To delete a restore point, use the *rm -rf* command. It is more safer if renamed it first, avoid the accidental use of a broken restore point.

```sh
cd /storage/directory
sudo mv SB01_* SB00_
sudo rm -rf SB00_
```
